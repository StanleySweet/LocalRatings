/**
 * This class helps in reducing the space occupied by a replay object in the database
 */
class LocalRatingsMinifier
{

    constructor()
    {
        // These keys MUST be the same as in Replay.js
        this.replayKeys = [
            "duration",
            "directory",
            "date",
            "players",
            "civs",
            "teamsSize",
            "startingResources",
            "populationCap",
            "worldPopulation",
            "hasAiPlayers",
            "cheatsEnabled",
            "revealedMap",
            "exploredMap",
            "nomad",
            "mods",
            "isValid",
            "scores"
        ];

        // These keys MUST be the same as in Sequences.js
        this.scoreKeys = [
            "resourcesGathered",
            "resourcesUsed",
            "resourcesBought",
            "resourcesSold",
            "tributesSent",
            "tradeIncome",
            "enemyUnitsKilledValue",
            "enemyUnitsKilled",
            "unitsCapturedValue",
            "unitsCaptured",
            "enemyBuildingsDestroyedValue",
            "enemyBuildingsDestroyed",
            "buildingsCapturedValue",
            "buildingsCaptured",
            "percentMapExplored",
            "percentMapControlled",
            "peakPercentMapControlled",
            "successfulBribes"
        ];

        this.civs = Object.keys(loadCivData());
    }

    minifyNumber(number)
    {
        return Math.round(number * 1e4) / 1e4;
    }

    minifyReplay(replayObj)
    {
        return this.replayKeys.map(replayKey => {
            if (replayKey == "scores")
            {
                return replayObj[replayKey].map((playerScores, playerIndex) =>
                    this.scoreKeys.map(scoreKey =>
                        this.minifyNumber(replayObj[replayKey][playerIndex][scoreKey])
                    )
                );
            }
            if (replayKey == "civs")
                return replayObj[replayKey].map(x => this.civs.indexOf(x));
            if (["worldPopulation", "hasAiPlayers", "cheatsEnabled", "revealedMap", "exploredMap", "nomad", "isValid"].includes(replayKey))
                return replayObj[replayKey] ? 1 : 0;
            return replayObj[replayKey];
        });
    }

    magnifyReplay(minifiedReplayObj)
    {
        return Object.fromEntries(this.replayKeys.map((replayKey, replayKeyIndex) => {
            if (replayKey == "scores")
            {
                return [replayKey, minifiedReplayObj[replayKeyIndex].map((playerScores, playerIndex) =>
                    Object.fromEntries(this.scoreKeys.map((scoreKey, scoreKeyIndex) =>
                        [scoreKey, minifiedReplayObj[replayKeyIndex][playerIndex][scoreKeyIndex]]
                    ))
                )];
            }
            if (replayKey == "civs")
                return [replayKey, minifiedReplayObj[replayKeyIndex].map(x => this.civs[x])];
            if (["worldPopulation", "hasAiPlayers", "cheatsEnabled", "revealedMap", "exploredMap", "nomad", "isValid"].includes(replayKey))
                return [replayKey, minifiedReplayObj[replayKeyIndex] ? true : false];
            return [replayKey, minifiedReplayObj[replayKeyIndex]];
        }));
    }

    minifyReplayDatabase(replayDatabase)
    {
        let minifiedReplayDatabase = {};
        for (const key in replayDatabase)
            minifiedReplayDatabase[key] = this.minifyReplay(replayDatabase[key]);
        return minifiedReplayDatabase;
    }

    magnifyReplayDatabase(minifiedReplayDatabase)
    {
        let replayDatabase = {};
        for (const key in minifiedReplayDatabase)
            replayDatabase[key] = this.magnifyReplay(minifiedReplayDatabase[key]);
        return replayDatabase;
    }

    minifyRatingsDatabase(ratingsDatabase)
    {
        let minifiedRatingsDatabase = {};
        for (const player in ratingsDatabase)
        {
            const playerData = ratingsDatabase[player];
            minifiedRatingsDatabase[player] = [this.minifyNumber(playerData.rating), playerData.matches];
        }
        return minifiedRatingsDatabase;
    }

    magnifyRatingsDatabase(minifiedRatingsDatabase)
    {
        let ratingsDatabase = {};
        for (const player in minifiedRatingsDatabase)
        {
            const playerData = minifiedRatingsDatabase[player];
            ratingsDatabase[player] = {"rating": playerData[0], "matches": playerData[1]};
        }
        return ratingsDatabase;
    }

    minifyHistoryDatabase(historyDatabase)
    {
        let minifiedHistoryDatabase = {};
        for (const player in historyDatabase)
        {
            let playerData = {};
            for (const replayDir in historyDatabase[player])
            {
                const replayData = historyDatabase[player][replayDir];
                playerData[replayDir] = [this.minifyNumber(replayData.rating), this.civs.indexOf(replayData.civ)];
            }
            minifiedHistoryDatabase[player] = playerData;
        }
        return minifiedHistoryDatabase;
    }

    magnifyHistoryDatabase(minifiedHistoryDatabase)
    {
        let historyDatabase = {};
        for (const player in minifiedHistoryDatabase)
        {
            let playerData = {};
            for (const replayDir in minifiedHistoryDatabase[player])
            {
                const replayData = minifiedHistoryDatabase[player][replayDir];
                playerData[replayDir] = {"rating": replayData[0], "civ": this.civs[replayData[1]]};
            }
            historyDatabase[player] = playerData;
        }
        return historyDatabase;
    }

}
