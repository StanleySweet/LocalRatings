/**
 * This class is responsible for updating the aliases database stored in the cache folder
 * and for updating ratings and history databases bases on alias association.
 */
class LocalRatingsAlias
{

    constructor()
    {
        this.aliasesDatabase = {};
    }

    load()
    {
        const cache = new LocalRatingsCache();
        this.aliasesDatabase = cache.load("aliasesDatabase");
    }

    save()
    {
        const cache = new LocalRatingsCache();
        cache.save("aliasesDatabase", this.aliasesDatabase);
    }

    merge(ratingsDatabase, historyDatabase)
    {
        if (!ratingsDatabase && !historyDatabase)
            return;

        this.load();

        const database = !historyDatabase ? ratingsDatabase : historyDatabase;
        const groups = Object.entries(this.aliasesDatabase)
              .map(([primary, aliases]) =>
                  [primary, ...aliases]
                      .filter(x => x in database)
                      .sort(sortString_LocalRatings))
              .filter(x => x.length > 0);

        // We merge aliases based on the first identity of the group, alphabetically ordered.
        // This ensures that the order of replay keys (YYYY-MM-DD_XXXX) follows this order, and is
        // independent on who is the primary identity.
        // This affects, for example, the chart representation, where the order of replays matters.
        for (const group of groups)
        {
            const first = group[0];
            const aliases = group.slice(1);
            let duplicates = {};
            for (const alias of aliases)
            {
                // If ratingsDatabase is defined, merge information on first
                if (!!ratingsDatabase)
                {
                    ratingsDatabase[first].rating = (ratingsDatabase[first].rating * ratingsDatabase[first].matches + ratingsDatabase[alias].rating * ratingsDatabase[alias].matches) / (ratingsDatabase[first].matches + ratingsDatabase[alias].matches);
                    ratingsDatabase[first].matches += ratingsDatabase[alias].matches;
                }
                // If historyDatabase is defined, merge information on first
                if (!!historyDatabase)
                {
                    // Change keys for duplicates. This occurs when multiple aliases are in the same game
                    Object.keys(historyDatabase[first])
                        .filter(x => Object.keys(historyDatabase[alias]).includes(x))
                        .forEach(x => {
                            (x in duplicates) ? duplicates[x] += 1 : duplicates[x] = 1;
                            historyDatabase[alias][x + "_" + duplicates[x]] = historyDatabase[alias][x];
                            delete historyDatabase[alias][x];
                        });
                    Object.assign(historyDatabase[first], historyDatabase[alias]);
                }
            }
            for (const alias of aliases)
            {
                // If ratingsDatabase is defined, copy information on aliases
                if (!!ratingsDatabase)
                    ratingsDatabase[alias] = ratingsDatabase[first];
                // If historyDatabase is defined, copy information on aliases
                if (!!historyDatabase)
                    historyDatabase[alias] = historyDatabase[first];
            }
        }
    }

    removeDuplicates(ratingsDatabase, historyDatabase)
    {
        this.load();
        Object.values(this.aliasesDatabase).flat().forEach(alias => {
            delete ratingsDatabase[alias];

            if (!!historyDatabase)
                delete historyDatabase[alias];
        });
    }

}
