class LocalRatingsButton
{

    onPress()
    {
	Engine.PushGuiPage("page_localratings.xml", { "dialog": true }, this.callback.bind(this));
    }

    callback()
    {
        if (Engine.ConfigDB_GetValue("user", "localratings.general.showplayerprofile") === "true")
        {
            let ratingsDatabase = init_LocalRatings();
            let alias = new LocalRatingsAlias();
            alias.merge(ratingsDatabase, undefined);
            g_LocalRatingsDatabase = ratingsDatabase;

            // Update player profile
            const selectedPlayer = g_LobbyHandler.lobbyPage.lobbyPage.panels.playerList.selectedPlayer;
            if (!!selectedPlayer)
                g_LobbyHandler.lobbyPage.lobbyPage.panels.profilePanel.onLeaderboardSelectionChange(selectedPlayer);

            // Update game description
            g_LobbyHandler.lobbyPage.lobbyPage.panels.gameList.onSelectionChange();

            // Update profile page
            if (Engine.GetProfile().length > 0)
                g_LobbyHandler.profilePage.onProfile();
        }
    }

}
