MenuButtons.prototype.LocalRatings = class
{

    constructor(button)
    {
        if (!button)
            return;

	this.button = button;
	this.button.caption = translate("Local Ratings");
	this.button.hotkey = "localratings.dialog";
	this.button.enabled = true;

	registerHotkeyChangeHandler(this.rebuild.bind(this));
    }

    rebuild()
    {
        if (!this.button)
            return;

	this.button.tooltip = sprintf(translate("Press %(hotkey)s to open the Local Ratings page without leaving the game."), {
	    "hotkey": colorizeHotkey("%(hotkey)s", this.button.hotkey),
	});
    }

    onPress()
    {
	closeOpenDialogs();
	Engine.PushGuiPage("page_localratings.xml", { "dialog": true });
    }

};
