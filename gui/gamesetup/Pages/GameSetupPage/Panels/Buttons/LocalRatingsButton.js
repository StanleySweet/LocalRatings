class LocalRatingsButton
{

    onPress()
    {
	Engine.PushGuiPage("page_localratings.xml", {"dialog": true}, this.callback.bind(this));
    }

    callback()
    {
        if (Engine.ConfigDB_GetValue("user", "localratings.general.showmatchsetup") === "true")
        {
            let ratingsDatabase = init_LocalRatings();
            let alias = new LocalRatingsAlias();
            alias.merge(ratingsDatabase, undefined);
            g_LocalRatingsDatabase = ratingsDatabase;
        }
        g_SetupWindow.pages.GameSetupPage.gameSettingControlManager.playerSettingControlManagers.forEach(x => x.playerSettingControls.PlayerAssignment.rebuildList());
    }

}
