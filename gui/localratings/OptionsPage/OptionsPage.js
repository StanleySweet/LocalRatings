var g_LocalRatingsInitialSettings;
var g_LocalRatingsDynamicOptions = {};

function init(data, hotloadData)
{
    g_ChangedKeys = new Set();

    // Save default settings if they don't exist
    const settings = new LocalRatingsSettings();
    settings.createDefaultSettingsIfNotExist();
    g_LocalRatingsInitialSettings = settings.getSaved();

    // Overwrite options
    g_Options = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json");
    translateObjectKeys(g_Options, ["label", "tooltip"]);

    placeTabButtons(
	g_Options,
	false,
	g_TabButtonHeight,
	g_TabButtonDist,
	selectPanel,
	displayOptions
    );

    // Initialize dynamic options
    g_LocalRatingsDynamicOptions = {
        3: new ModFilterOptions()
    };
}

displayOptions = new Proxy(displayOptions, {apply: function(target, thisArg, args) {
    // This must go here
    if (g_TabCategorySelected in g_LocalRatingsDynamicOptions)
        g_LocalRatingsDynamicOptions[g_TabCategorySelected].display();
    else
    {
        let optionControls = Engine.GetGUIObjectByName("option_controls");
        let optionControlsSize = optionControls.size;
        optionControlsSize.bottom = -52;
        optionControls.size = optionControlsSize;
        Engine.GetGUIObjectByName("navigationBox").hidden = true;
    }
    // Run original function
    target(...args);
    // This must go here
    if (g_TabCategorySelected in g_LocalRatingsDynamicOptions)
        g_LocalRatingsDynamicOptions[g_TabCategorySelected].set();
}});

function reallySetDefaults()
{
    const settings = new LocalRatingsSettings();
    const restoredSettings = settings.restoreLocalDefault();
    g_ChangedKeys = new Set([...Object.keys(restoredSettings)]);
    revertChanges();
}

closePageWithoutConfirmation = new Proxy(closePageWithoutConfirmation, {apply: function(target, thisArg, args) {
    // Reset changes when present, so they won't get saved
    if (Engine.ConfigDB_HasChanges("user"))
    {
        Engine.ConfigDB_Reload("user");
        g_ChangedKeys = new Set();
    }
    // If there are changes, remove keys that have not been changed from the list of changed keys
    else
        Object.keys(g_LocalRatingsInitialSettings)
        .filter(key => Engine.ConfigDB_GetValue("user", key) === g_LocalRatingsInitialSettings[key])
        .forEach(key => g_ChangedKeys.delete(key));
    // Run original function
    target(...args);
}});
