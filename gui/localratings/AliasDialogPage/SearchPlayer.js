class LocalRatingsAliasDialogPageSearchPlayer
{

    constructor(aliasDialogPage)
    {
        // Arguments
        this.aliasDialogPage = aliasDialogPage;

        // GUI objects
        this.searchPlayerInput = Engine.GetGUIObjectByName("searchPlayerInput");

        // Events
        this.searchPlayerInput.onTextEdit = this.onSearchPlayerInputTextEdit.bind(this);
    }

    onSearchPlayerInputTextEdit()
    {
        this.aliasDialogPage.onSearchEdit();
    }

}
