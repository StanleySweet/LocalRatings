class LocalRatingsEvolutionChart extends LocalRatingsChart
{

    constructor(...args)
    {
        super(...args);

        // GUI objects
        this.chartContainer = Engine.GetGUIObjectByName("evolutionChartContainer");
        this.chartCanvas = Engine.GetGUIObjectByName("evolutionChartCanvas");
        this.chartXAxisLabel = Engine.GetGUIObjectByName("evolutionChartXAxisLabel");
        this.chartMarker = Engine.GetGUIObjectByName("evolutionMarker");

        // Events
        this.chartCanvas.onMouseMove = this.onChartCanvasMouseMove.bind(this);
        this.chartCanvas.onMouseLeave = this.onChartCanvasMouseLeave.bind(this);

        // Options
        this.configOptions = new LocalRatingsEvolutionChartOptions().configOptions;

        // Chart helpers
        const civData = loadCivData();
        this.civs = Object.fromEntries(Object.keys(civData).map(x => [x, civData[x].Name]));
        this.series;
        this.colors;
        this.chartCorners;

        // Data
        this.singleGamesRatings;
        this.averageRatings;
        this.currentRating;
        this.dataSize;
    }

    onChartCanvasMouseMove(mouse)
    {
        if (!this.dataSize)
            return;

        // Get index according to mouse position
        const size = this.chartCanvas.getComputedSize();
        const interval = (size.right - size.left) / (this.dataSize - 1);
        const index = Math.round((mouse.x - size.left) / interval);

        let tooltipData = [];
        const replayKey = Object.keys(this.playerData).sort()[index];
        // Add game civ
        const civName = this.civs[this.playerData[replayKey].civ];
        tooltipData.push(civName);
        // Add current rating
        if (this.configOptions.showcurrent)
            tooltipData.push(coloredText("■ ", this.configOptions.colorcurrent) + formatRating_LocalRatings(this.currentRating));
        // Add game rating
        if (this.configOptions.showevolution)
            tooltipData.push(coloredText("■ ", this.configOptions.colorevolution) + formatRating_LocalRatings(this.averageRatings[index]));
        // Add game performance
        if (this.configOptions.showperformance)
            tooltipData.push(coloredText("■ ", this.configOptions.colorperformance) + formatRating_LocalRatings(this.singleGamesRatings[index]));
        // Add replay
        tooltipData.push(translate("Replay") + ": " + replayKey);
        // Set tooltip
        this.chartCanvas.tooltip = tooltipData.join("\n");

        // Marker
        if (this.configOptions.showmarker)
        {
            let chartMarkerSize = this.chartMarker.size;
            chartMarkerSize.left = interval*index;
            chartMarkerSize.right = interval*index + 2;
            this.chartMarker.size = chartMarkerSize;
            this.chartMarker.hidden = false;
        }
    }

    onChartCanvasMouseLeave(mouse)
    {
        this.chartMarker.hidden = true;
    }

    getSingleGamesRatings()
    {
        return Object.keys(this.playerData).sort().map(x => this.playerData[x].rating);
    }

    getAverageRatings(singleGamesRatings)
    {
        let singleGamesRatingsSum = [singleGamesRatings[0]];
        for (let i=1; i<singleGamesRatings.length; i++)
            singleGamesRatingsSum.push(singleGamesRatingsSum[i-1] + singleGamesRatings[i]);
        return singleGamesRatingsSum.map((x, i) => x / (i+1));
    }

    getChartCorners(series)
    {
        let corners = {
            "xmin": Math.min(...series.map(c => Math.min(...c.map(d => d.x)))),
            "xmax": Math.max(...series.map(c => Math.max(...c.map(d => d.x)))),
            "ymin": Math.min(...series.map(c => Math.min(...c.map(d => d.y)))),
            "ymax": Math.max(...series.map(c => Math.max(...c.map(d => d.y))))
        };
        if (corners.ymin == corners.ymax)
        {
            corners.ymin -= 1;
            corners.ymax += 1;
        }
        return corners;
    }

    animate(...args)
    {
        if (this.series === undefined)
            return;

        const step = args[0];

        this.chartCanvas.series = this.series.map(c => c.map(d => ({"x": d.x, "y": d.y*step}))).concat([[{"x": this.chartCorners.xmin, "y": this.chartCorners.ymin}, {"x": this.chartCorners.xmax, "y": this.chartCorners.ymax}]]);
        this.chartCanvas.series_color = this.colors.concat(["0 0 0 0"]);
    }

    draw(...args)
    {
        // Retrieve relevant data
        this.singleGamesRatings = this.getSingleGamesRatings();
        this.averageRatings = this.getAverageRatings(this.singleGamesRatings);
        this.dataSize = this.singleGamesRatings.length;
        this.currentRating = this.averageRatings[this.dataSize-1];
        // Initialize series, colors and legend
        let series = [];
        let colors = [];
        let legend = [];
        // Push to GUI
        if (this.configOptions.showzero)
        {
            const zeroLineDataSet = (this.dataSize == 1) ?
                  [{"x": 0, "y": 0}, {"x": 1, "y": 0}] :
                  [{"x": 1, "y": 0}, {"x": this.dataSize, "y": 0}];
            series.push(zeroLineDataSet);
            colors.push(this.configOptions.colorzero);
            legend.push(coloredText("■ " + translate("Zero Line"), this.configOptions.colorzero));
        }
        if (this.configOptions.showcurrent)
        {
            const currentRatingDataSet = (this.dataSize == 1) ?
                  [{"x": 0, "y": this.currentRating*100}, {"x": 1, "y": this.currentRating*100}] :
                  [{"x": 1, "y": this.currentRating*100}, {"x": this.dataSize, "y": this.currentRating*100}];
            series.push(currentRatingDataSet);
            colors.push(this.configOptions.colorcurrent);
            legend.push(coloredText("■ " + translate("Current rating"), this.configOptions.colorcurrent));
        }
        if (this.configOptions.showevolution)
        {
            const averageRatingsDataSet = (this.dataSize == 1) ?
                  [{"x": 0, "y": this.currentRating*100}, {"x": 1, "y": this.currentRating*100}] :
                  this.averageRatings.map((y, i) => ({"x": i+1, "y": y*100}));
            series.push(averageRatingsDataSet);
            colors.push(this.configOptions.colorevolution);
            legend.push(coloredText("■ " + translate("Rating evolution"), this.configOptions.colorevolution));
        }
        if (this.configOptions.showperformance)
        {
            const singleGamesRatingsDataSet = (this.dataSize == 1) ?
                  [{"x": 0, "y": this.currentRating*100}, {"x": 1, "y": this.currentRating*100}] :
                  this.singleGamesRatings.map((y, i) => ({"x": i+1, "y": y*100}));
            series.push(singleGamesRatingsDataSet);
            colors.push(this.configOptions.colorperformance);
            legend.push(coloredText("■ " + translate("Performance over time"), this.configOptions.colorperformance));
        }

        this.chartLegend.caption = legend.join("   ");
        this.chartLegend.tooltip = translate("Colors and number of bins can be changed from the Options menu.");
        this.chartMarker.sprite = "color: " + this.configOptions.colormarker;
        this.series = series;
        this.colors = colors;
        this.chartCorners = this.getChartCorners(series);
        this.currentAnimation = 1;

        super.draw();
    }

}
