class LocalRatingsTabs
{

    constructor(localRatingsPage)
    {
        // Arguments
        this.localRatingsPage = localRatingsPage;

        // GUI objects
        this.tabEvolutionChart = Engine.GetGUIObjectByName("tabEvolutionChart");
        this.tabEvolutionChartButton = Engine.GetGUIObjectByName("tabEvolutionChartButton");
        this.tabCivilizationChart = Engine.GetGUIObjectByName("tabCivilizationChart");
        this.tabCivilizationChartButton = Engine.GetGUIObjectByName("tabCivilizationChartButton");
        this.tabDistributionChart = Engine.GetGUIObjectByName("tabDistributionChart");
        this.tabDistributionChartButton = Engine.GetGUIObjectByName("tabDistributionChartButton");
        this.tabDividerLeft = Engine.GetGUIObjectByName("tabDividerLeft");
        this.tabDividerRight = Engine.GetGUIObjectByName("tabDividerRight");

        // Events
        this.tabEvolutionChartButton.onPress = this.onTabEvolutionChartButtonPress.bind(this);
        this.tabCivilizationChartButton.onPress = this.onTabCivilizationChartButtonPress.bind(this);
        this.tabDistributionChartButton.onPress = this.onTabDistributionChartButtonPress.bind(this);

        // Other
        this.tabs = {
            "evolution": this.tabEvolutionChart,
            "civilization": this.tabCivilizationChart,
            "distribution": this.tabDistributionChart
        };
        this.selectedTab = "evolution";
        this.update();
    }

    onTabEvolutionChartButtonPress()
    {
        this.selectedTab = "evolution";
        this.update();
        this.localRatingsPage.updateChartFrame();
    }

    onTabCivilizationChartButtonPress()
    {
        this.selectedTab = "civilization";
        this.update();
        this.localRatingsPage.updateChartFrame();
    }

    onTabDistributionChartButtonPress()
    {
        this.selectedTab = "distribution";
        this.update();
        this.localRatingsPage.updateChartFrame();
    }

    getSelectedTab()
    {
        return this.selectedTab;
    }

    update()
    {
        // Set sprite
        let tab = this.tabs[this.selectedTab];
        Object.values(this.tabs).forEach(x => x.sprite = "ModernTabHorizontalBackground");
        tab.sprite = "ModernTabHorizontalForeground";

        // Set divider's left size
        let tabDividerLeftSize = this.tabDividerLeft.size;
        tabDividerLeftSize.rright = tab.size.rleft;
        this.tabDividerLeft.size = tabDividerLeftSize;

        // Set divider's right sixe
        let tabDividerRightSize = this.tabDividerRight.size;
        tabDividerRightSize.rleft = tab.size.rright;
        this.tabDividerRight.size = tabDividerRightSize;
    }

}
