class LocalRatingsButtonsPanel
{

    constructor(localRatingsPage)
    {
        // Arguments
        this.localRatingsPage = localRatingsPage;

        // GUI objects
        this.mainMenuButton = Engine.GetGUIObjectByName("mainMenuButton");
        this.optionsButton = Engine.GetGUIObjectByName("optionsButton");
        this.aliasButton = Engine.GetGUIObjectByName("aliasButton");
        this.rebuildButton = Engine.GetGUIObjectByName("rebuildButton");

        // Events
        this.mainMenuButton.onPress = this.onMainMenuButtonPress.bind(this);
        this.optionsButton.onPress = this.onOptionsButtonPress.bind(this);
        this.aliasButton.onPress = this.onAliasButtonPress.bind(this);
        this.rebuildButton.onPress = this.onRebuildButtonPress.bind(this);
    }

    onMainMenuButtonPress()
    {
        this.localRatingsPage.saveSettings();
        (this.localRatingsPage.dialog) ? Engine.PopGuiPage() : Engine.SwitchGuiPage("page_pregame.xml");
    }

    onOptionsButtonPress()
    {
        Engine.PushGuiPage("page_localratings_options.xml", {}, data => {
            const changes = Array.from(data);
            const prefix = "localratings."

            // These keys require database update and list update.
            // List update automatically triggers chart update.
            if (changes.some(x => ["filter", "modfilter", "weight"].some(y => x.startsWith(prefix + y))))
                this.localRatingsPage.rebuildRatings();

            // These keys only require list update and not database update.
            // List update automatically triggers chart update.
            else if (changes.some(x => ["playerfilter"].some(y => x.startsWith(prefix + y))))
            this.localRatingsPage.updatePlayerList();

            // These keys only require evolution chart update and no list or database update.
            else if (changes.some(x => ["charts"].some(y => x.startsWith(prefix + y)) && this.localRatingsPage.tabs.getSelectedTab() == "evolution"))
                this.localRatingsPage.updateChartFrame();

            // These keys only require distribution chart update and no list or database update.
            else if (changes.some(x => ["distribution"].some(y => x.startsWith(prefix + y)) && this.localRatingsPage.tabs.getSelectedTab() == "distribution"))
                this.localRatingsPage.updateChartFrame();
        });
    }

    onAliasButtonPress()
    {
        // Load aliases database, to detect changes
        let alias = new LocalRatingsAlias();
        alias.load();

        Engine.PushGuiPage("page_localratings_alias.xml", {
            "players": Object.keys(this.localRatingsPage.ratingsDatabase)
        }, data => {
            // If changes are detected, update the player list
            const oldDB = alias.aliasesDatabase;
            const newDB = data.aliases;
            const oldKeys = Object.keys(oldDB);
            const newKeys = Object.keys(newDB);
            const hasChanged = oldKeys.length !== newKeys.length ||
                  oldKeys.some(key => !newDB.hasOwnProperty(key)) ||
                  oldKeys.some(key => JSON.stringify(oldDB[key]) !== JSON.stringify(newDB[key]));
            if (hasChanged)
                this.localRatingsPage.updatePlayerList();
        });
    }

    onRebuildButtonPress()
    {
        Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "Rebuild list",
            "text": [
                "The list of players will be rebuilt from scratch.",
                "Rebuild is only needed if replay files have been deleted or modified.",
                "This may take a while. Click on \"Rebuild\" to update."
            ],
            "cancel": "Cancel",
            "confirm": "Rebuild"
        }, data => {
            if (data.confirm)
                this.localRatingsPage.rebuildReplays();
        });
    }

    setDialogStyle()
    {
        this.mainMenuButton.caption = translate("Close Dialog");
        Engine.SetGlobalHotkey("localratings.dialog", "Press", this.onMainMenuButtonPress.bind(this));
    }

}
