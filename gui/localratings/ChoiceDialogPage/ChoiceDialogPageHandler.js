class LocalRatingsChoiceDialogPage
{

    constructor(data)
    {
        // GUI objects
        this.textLabel = Engine.GetGUIObjectByName("textLabel");
        this.titleLabel = Engine.GetGUIObjectByName("titleLabel");
        this.cancelButton = Engine.GetGUIObjectByName("cancelButton");
        this.confirmButton = Engine.GetGUIObjectByName("confirmButton");

        // Events
        this.cancelButton.onPress = this.onPressCancelButton.bind(this);
        this.confirmButton.onPress = this.onPressConfirmButton.bind(this);

        this.init(data);
    }

    onPressCancelButton()
    {
        Engine.PopGuiPage({"confirm": false});
    }

    onPressConfirmButton()
    {
        Engine.PopGuiPage({"confirm": true});
    }

    init(data)
    {
        // Title
        if (data && data.title)
            this.titleLabel.caption = translate(data.title);
        // Text
        if (data && data.text)
            this.textLabel.caption = data.text.map(x => translate(x)).join("\n\n");
        // Cancel button
        if (data && data.cancel)
            this.cancelButton.caption = translate(data.cancel);
        else
            this.cancelButton.hidden = true;
        // Confirm button
        if (data && data.confirm)
            this.confirmButton.caption = translate(data.confirm);
        else
            this.confirmButton.hidden = true;
    }

}
