function init(data)
{
    if (!data || !("players" in data))
        return;

    new LocalRatingsAliasPage(data.players);
}
